
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jozkar
 */
public class Vehicle extends Agent {
        
    private String nextLight, cityID, me;
    protected final String SEMAPHORE_AGENT_NAME = "Light";
    private int ways = 3,
                priority,
                transitionDelay,  // doba potrebna pro prujezd krizovatkou
                STRAIGHT = 1,
                noJunctions = 0;
    private long time = 0;
    private Random random;
    
    protected void setup() {
  	//System.out.println("Hello World! I am "+getLocalName()+ " city!");
        random = new Random();
        
        Object[] args = getArguments();
        priority = ((Integer)args[0]).intValue();
        cityID = (String)args[1];
        me = getAID().getLocalName();
        transitionDelay = random.nextInt(800); //TODO? maximalni zpozdeni je 1s
        time = System.currentTimeMillis();
        addBehaviour(new Vehicle.RcvVehicleMsgBehaviour());
    }
    
    class RcvVehicleMsgBehaviour extends CyclicBehaviour{
        @Override
        public void action(){
            ACLMessage recive = myAgent.receive();
            if(recive != null){
                
                /**  TYP_ZPRAVY(odesilatel): vyznam
                 * INFORM(city): zarad se do fronty k udanemu semaforu - jen u inicializace
                 * CANCEL(semafor): opust semafor
                 * AGREE(semafor): novy semafor po ceste
                */
                
                if(recive.getPerformative() == ACLMessage.INFORM){
                    nextLight = SEMAPHORE_AGENT_NAME + recive.getContent();
                    int way = (priority == 1? random.nextInt(ways) :  STRAIGHT); 
                    ACLMessage toLight = new ACLMessage(ACLMessage.SUBSCRIBE);
                    toLight.setContent(way + ";" + priority);
                    toLight.addReceiver(getAID(nextLight));
                    myAgent.send(toLight);
                   // System.out.println(me + " is in queue to trafficlight " + nextLight);
                }
                
                if(recive.getPerformative() == ACLMessage.CANCEL){
                   // System.out.println(me + ": Driving through junction");
                    noJunctions++;
                    //block(transitionDelay); 
                    try {
                        Thread.sleep(transitionDelay); // cas potrebny na prujezd
                    }
                    catch(Exception e) {
                        System.out.println(e.getMessage());
                    }
                    //System.out.println("Waiting for " + (System.currentTimeMillis() - time) + "ms");
                    
                    ACLMessage toOldLight = new ACLMessage(ACLMessage.CONFIRM);
                    toOldLight.addReceiver(recive.getSender());
                    toOldLight.setContent(priority + "");
                    myAgent.send(toOldLight);
                    //System.out.println(me + " is leaving old queue " + nextLight);
                    
                    if(nextLight.equals("exit")){
                        ACLMessage toCity = new ACLMessage(ACLMessage.CANCEL);
                        long diff = System.currentTimeMillis() - time;
                        //System.out.println(diff);
                        toCity.setContent(priority + ";" + noJunctions + ";" + diff);
                        toCity.addReceiver(getAID(cityID));
                        myAgent.send(toCity);
                        myAgent.doDelete();
                        
                    }else{
                        int way = (priority == 1? random.nextInt(ways) :  STRAIGHT);
                        ACLMessage toLight = new ACLMessage(ACLMessage.SUBSCRIBE);
                        toLight.addReceiver(getAID(nextLight));
                        toLight.setContent(way + ";" + priority);
                        myAgent.send(toLight);
                       // System.out.println(me + " is in queue to trafficlight " + nextLight);
                    }
                }
                
                if(recive.getPerformative() == ACLMessage.AGREE){
                    nextLight = recive.getContent();
                    //System.out.println(me + " is reading next trafficlight id " + recive.getSender().getLocalName());
                }
            }
        }
    }
    
}
