import java.awt.*;
import javax.swing.*;

/**
 *
 * @author Supinic
 */
public class GUICityClass extends JFrame {
   private City myAgent;
   private JPanel junctionPanels[];
   private JPanel junctionSignPanels[];
   private JTextArea  junctionTexts[];
   private int junction_max;
   
   public GUICityClass (City a, int junctions) {     
      init_components();
      myAgent = a;      // Reference to class BankClientAgent
      junction_max = junctions; 
      
      junctionPanels = new JPanel[junctions];
      junctionTexts = new JTextArea[junctions];
      
      junctionPanels[0] = junctionPanel0; junctionPanels[1] = junctionPanel1; junctionPanels[2] = junctionPanel2;
      junctionPanels[3] = junctionPanel3; junctionPanels[4] = junctionPanel4; junctionPanels[5] = junctionPanel5;
      junctionPanels[6] = junctionPanel6; junctionPanels[7] = junctionPanel7; junctionPanels[8] = junctionPanel8;
         
      junctionTexts[0] = junctionTextField0; junctionTexts[1] = junctionTextField1; junctionTexts[2] = junctionTextField2; 
      junctionTexts[3] = junctionTextField3; junctionTexts[4] = junctionTextField4; junctionTexts[5] = junctionTextField5;
      junctionTexts[6] = junctionTextField6; junctionTexts[7] = junctionTextField7; junctionTexts[8] = junctionTextField8; 
      
      junctionSignPanels = new JPanel[junctions * 4];
   
      junctionSignPanels[0] = junctionSignPanel0_N; junctionSignPanels[1] = junctionSignPanel0_W; junctionSignPanels[2] = junctionSignPanel0_S; junctionSignPanels[3] = junctionSignPanel0_E;
      junctionSignPanels[4] = junctionSignPanel1_N; junctionSignPanels[5] = junctionSignPanel1_W; junctionSignPanels[6] = junctionSignPanel1_S; junctionSignPanels[7] = junctionSignPanel1_E;
      junctionSignPanels[8] = junctionSignPanel2_N; junctionSignPanels[9] = junctionSignPanel2_W; junctionSignPanels[10]= junctionSignPanel2_S; junctionSignPanels[11]= junctionSignPanel2_E;   
      junctionSignPanels[12]= junctionSignPanel3_N; junctionSignPanels[13]= junctionSignPanel3_W; junctionSignPanels[14]= junctionSignPanel3_S; junctionSignPanels[15]= junctionSignPanel3_E;
      junctionSignPanels[16]= junctionSignPanel4_N; junctionSignPanels[17]= junctionSignPanel4_W; junctionSignPanels[18]= junctionSignPanel4_S; junctionSignPanels[19]= junctionSignPanel4_E;
      junctionSignPanels[20]= junctionSignPanel5_N; junctionSignPanels[21]= junctionSignPanel5_W; junctionSignPanels[22]= junctionSignPanel5_S; junctionSignPanels[23]= junctionSignPanel5_E;      
      junctionSignPanels[24]= junctionSignPanel6_N; junctionSignPanels[25]= junctionSignPanel6_W; junctionSignPanels[26]= junctionSignPanel6_S; junctionSignPanels[27]= junctionSignPanel6_E;
      junctionSignPanels[28]= junctionSignPanel7_N; junctionSignPanels[29]= junctionSignPanel7_W; junctionSignPanels[30]= junctionSignPanel7_S; junctionSignPanels[31]= junctionSignPanel7_E;
      junctionSignPanels[32]= junctionSignPanel8_N; junctionSignPanels[33]= junctionSignPanel8_W; junctionSignPanels[34]= junctionSignPanel8_S; junctionSignPanels[35]= junctionSignPanel8_E;
      
      junctionVehicleField.setText("");
      
      pack();      
      setLocationRelativeTo(null);
    }
   
   public void init_components() {     

        junctionPanel0 = new javax.swing.JPanel();
        junctionTextField0 = new javax.swing.JTextArea();
        junctionSignPanel0_N = new javax.swing.JPanel();
        junctionSignPanel0_W = new javax.swing.JPanel();
        junctionSignPanel0_S = new javax.swing.JPanel();
        junctionSignPanel0_E = new javax.swing.JPanel();
        junctionPanel1 = new javax.swing.JPanel();
        junctionTextField1 = new javax.swing.JTextArea();
        junctionSignPanel1_N = new javax.swing.JPanel();
        junctionSignPanel1_W = new javax.swing.JPanel();
        junctionSignPanel1_S = new javax.swing.JPanel();
        junctionSignPanel1_E = new javax.swing.JPanel();
        junctionPanel2 = new javax.swing.JPanel();
        junctionTextField2 = new javax.swing.JTextArea();
        junctionSignPanel2_N = new javax.swing.JPanel();
        junctionSignPanel2_W = new javax.swing.JPanel();
        junctionSignPanel2_S = new javax.swing.JPanel();
        junctionSignPanel2_E = new javax.swing.JPanel();
        junctionPanel3 = new javax.swing.JPanel();
        junctionTextField3 = new javax.swing.JTextArea();
        junctionSignPanel3_N = new javax.swing.JPanel();
        junctionSignPanel3_W = new javax.swing.JPanel();
        junctionSignPanel3_S = new javax.swing.JPanel();
        junctionSignPanel3_E = new javax.swing.JPanel();
        junctionPanel4 = new javax.swing.JPanel();
        junctionTextField4 = new javax.swing.JTextArea();
        junctionSignPanel4_N = new javax.swing.JPanel();
        junctionSignPanel4_W = new javax.swing.JPanel();
        junctionSignPanel4_S = new javax.swing.JPanel();
        junctionSignPanel4_E = new javax.swing.JPanel();
        junctionPanel5 = new javax.swing.JPanel();
        junctionTextField5 = new javax.swing.JTextArea();
        junctionSignPanel5_N = new javax.swing.JPanel();
        junctionSignPanel5_W = new javax.swing.JPanel();
        junctionSignPanel5_S = new javax.swing.JPanel();
        junctionSignPanel5_E = new javax.swing.JPanel();
        junctionPanel6 = new javax.swing.JPanel();
        junctionTextField6 = new javax.swing.JTextArea();
        junctionSignPanel6_N = new javax.swing.JPanel();
        junctionSignPanel6_W = new javax.swing.JPanel();
        junctionSignPanel6_S = new javax.swing.JPanel();
        junctionSignPanel6_E = new javax.swing.JPanel();
        junctionPanel7 = new javax.swing.JPanel();
        junctionTextField7 = new javax.swing.JTextArea();
        junctionSignPanel7_N = new javax.swing.JPanel();
        junctionSignPanel7_W = new javax.swing.JPanel();
        junctionSignPanel7_S = new javax.swing.JPanel();
        junctionSignPanel7_E = new javax.swing.JPanel();
        junctionPanel8 = new javax.swing.JPanel();
        junctionTextField8 = new javax.swing.JTextArea();
        junctionSignPanel8_N = new javax.swing.JPanel();
        junctionSignPanel8_W = new javax.swing.JPanel();
        junctionSignPanel8_S = new javax.swing.JPanel();
        junctionSignPanel8_E = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        junctionVehicleField = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        junctionTextField0.setEditable(false);
        junctionTextField0.setColumns(8);
        junctionTextField0.setText("Junction NW");
        junctionTextField0.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel0_NLayout = new javax.swing.GroupLayout(junctionSignPanel0_N);
        junctionSignPanel0_N.setLayout(junctionSignPanel0_NLayout);
        junctionSignPanel0_NLayout.setHorizontalGroup(
            junctionSignPanel0_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel0_NLayout.setVerticalGroup(
            junctionSignPanel0_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel0_WLayout = new javax.swing.GroupLayout(junctionSignPanel0_W);
        junctionSignPanel0_W.setLayout(junctionSignPanel0_WLayout);
        junctionSignPanel0_WLayout.setHorizontalGroup(
            junctionSignPanel0_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel0_WLayout.setVerticalGroup(
            junctionSignPanel0_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel0_SLayout = new javax.swing.GroupLayout(junctionSignPanel0_S);
        junctionSignPanel0_S.setLayout(junctionSignPanel0_SLayout);
        junctionSignPanel0_SLayout.setHorizontalGroup(
            junctionSignPanel0_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel0_SLayout.setVerticalGroup(
            junctionSignPanel0_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel0_ELayout = new javax.swing.GroupLayout(junctionSignPanel0_E);
        junctionSignPanel0_E.setLayout(junctionSignPanel0_ELayout);
        junctionSignPanel0_ELayout.setHorizontalGroup(
            junctionSignPanel0_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel0_ELayout.setVerticalGroup(
            junctionSignPanel0_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel0Layout = new javax.swing.GroupLayout(junctionPanel0);
        junctionPanel0.setLayout(junctionPanel0Layout);
        junctionPanel0Layout.setHorizontalGroup(
            junctionPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel0Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel0_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel0_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel0_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel0_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel0Layout.setVerticalGroup(
            junctionPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel0Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel0_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel0Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField0, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel0_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel0Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel0_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel0_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(72, Short.MAX_VALUE))
        );

        junctionTextField1.setEditable(false);
        junctionTextField1.setColumns(8);
        junctionTextField1.setText("Junction N");
        junctionTextField1.setPreferredSize(new java.awt.Dimension(70, 70));
        junctionTextField1.setRequestFocusEnabled(false);

        javax.swing.GroupLayout junctionSignPanel1_NLayout = new javax.swing.GroupLayout(junctionSignPanel1_N);
        junctionSignPanel1_N.setLayout(junctionSignPanel1_NLayout);
        junctionSignPanel1_NLayout.setHorizontalGroup(
            junctionSignPanel1_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel1_NLayout.setVerticalGroup(
            junctionSignPanel1_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel1_WLayout = new javax.swing.GroupLayout(junctionSignPanel1_W);
        junctionSignPanel1_W.setLayout(junctionSignPanel1_WLayout);
        junctionSignPanel1_WLayout.setHorizontalGroup(
            junctionSignPanel1_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel1_WLayout.setVerticalGroup(
            junctionSignPanel1_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel1_SLayout = new javax.swing.GroupLayout(junctionSignPanel1_S);
        junctionSignPanel1_S.setLayout(junctionSignPanel1_SLayout);
        junctionSignPanel1_SLayout.setHorizontalGroup(
            junctionSignPanel1_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel1_SLayout.setVerticalGroup(
            junctionSignPanel1_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel1_ELayout = new javax.swing.GroupLayout(junctionSignPanel1_E);
        junctionSignPanel1_E.setLayout(junctionSignPanel1_ELayout);
        junctionSignPanel1_ELayout.setHorizontalGroup(
            junctionSignPanel1_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel1_ELayout.setVerticalGroup(
            junctionSignPanel1_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel1Layout = new javax.swing.GroupLayout(junctionPanel1);
        junctionPanel1.setLayout(junctionPanel1Layout);
        junctionPanel1Layout.setHorizontalGroup(
            junctionPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel1Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel1_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel1_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel1_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel1_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel1Layout.setVerticalGroup(
            junctionPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel1Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel1_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel1_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel1Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel1_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel1_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField2.setEditable(false);
        junctionTextField2.setColumns(8);
        junctionTextField2.setText("Junction NE");
        junctionTextField2.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel2_NLayout = new javax.swing.GroupLayout(junctionSignPanel2_N);
        junctionSignPanel2_N.setLayout(junctionSignPanel2_NLayout);
        junctionSignPanel2_NLayout.setHorizontalGroup(
            junctionSignPanel2_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel2_NLayout.setVerticalGroup(
            junctionSignPanel2_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel2_WLayout = new javax.swing.GroupLayout(junctionSignPanel2_W);
        junctionSignPanel2_W.setLayout(junctionSignPanel2_WLayout);
        junctionSignPanel2_WLayout.setHorizontalGroup(
            junctionSignPanel2_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel2_WLayout.setVerticalGroup(
            junctionSignPanel2_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel2_SLayout = new javax.swing.GroupLayout(junctionSignPanel2_S);
        junctionSignPanel2_S.setLayout(junctionSignPanel2_SLayout);
        junctionSignPanel2_SLayout.setHorizontalGroup(
            junctionSignPanel2_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel2_SLayout.setVerticalGroup(
            junctionSignPanel2_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel2_ELayout = new javax.swing.GroupLayout(junctionSignPanel2_E);
        junctionSignPanel2_E.setLayout(junctionSignPanel2_ELayout);
        junctionSignPanel2_ELayout.setHorizontalGroup(
            junctionSignPanel2_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel2_ELayout.setVerticalGroup(
            junctionSignPanel2_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel2Layout = new javax.swing.GroupLayout(junctionPanel2);
        junctionPanel2.setLayout(junctionPanel2Layout);
        junctionPanel2Layout.setHorizontalGroup(
            junctionPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel2_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel2_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel2_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel2_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel2Layout.setVerticalGroup(
            junctionPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel2Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel2_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel2_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel2Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel2_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel2_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField3.setEditable(false);
        junctionTextField3.setColumns(8);
        junctionTextField3.setText("Junction W");
        junctionTextField3.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel3_NLayout = new javax.swing.GroupLayout(junctionSignPanel3_N);
        junctionSignPanel3_N.setLayout(junctionSignPanel3_NLayout);
        junctionSignPanel3_NLayout.setHorizontalGroup(
            junctionSignPanel3_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel3_NLayout.setVerticalGroup(
            junctionSignPanel3_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel3_WLayout = new javax.swing.GroupLayout(junctionSignPanel3_W);
        junctionSignPanel3_W.setLayout(junctionSignPanel3_WLayout);
        junctionSignPanel3_WLayout.setHorizontalGroup(
            junctionSignPanel3_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel3_WLayout.setVerticalGroup(
            junctionSignPanel3_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel3_SLayout = new javax.swing.GroupLayout(junctionSignPanel3_S);
        junctionSignPanel3_S.setLayout(junctionSignPanel3_SLayout);
        junctionSignPanel3_SLayout.setHorizontalGroup(
            junctionSignPanel3_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel3_SLayout.setVerticalGroup(
            junctionSignPanel3_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel3_ELayout = new javax.swing.GroupLayout(junctionSignPanel3_E);
        junctionSignPanel3_E.setLayout(junctionSignPanel3_ELayout);
        junctionSignPanel3_ELayout.setHorizontalGroup(
            junctionSignPanel3_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel3_ELayout.setVerticalGroup(
            junctionSignPanel3_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel3Layout = new javax.swing.GroupLayout(junctionPanel3);
        junctionPanel3.setLayout(junctionPanel3Layout);
        junctionPanel3Layout.setHorizontalGroup(
            junctionPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel3Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel3_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel3_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel3_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel3_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel3Layout.setVerticalGroup(
            junctionPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel3Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel3_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel3_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel3Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel3_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel3_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField4.setEditable(false);
        junctionTextField4.setColumns(8);
        junctionTextField4.setText("Junction C");
        junctionTextField4.setFocusCycleRoot(true);
        junctionTextField4.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel4_NLayout = new javax.swing.GroupLayout(junctionSignPanel4_N);
        junctionSignPanel4_N.setLayout(junctionSignPanel4_NLayout);
        junctionSignPanel4_NLayout.setHorizontalGroup(
            junctionSignPanel4_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel4_NLayout.setVerticalGroup(
            junctionSignPanel4_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel4_WLayout = new javax.swing.GroupLayout(junctionSignPanel4_W);
        junctionSignPanel4_W.setLayout(junctionSignPanel4_WLayout);
        junctionSignPanel4_WLayout.setHorizontalGroup(
            junctionSignPanel4_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel4_WLayout.setVerticalGroup(
            junctionSignPanel4_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel4_SLayout = new javax.swing.GroupLayout(junctionSignPanel4_S);
        junctionSignPanel4_S.setLayout(junctionSignPanel4_SLayout);
        junctionSignPanel4_SLayout.setHorizontalGroup(
            junctionSignPanel4_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel4_SLayout.setVerticalGroup(
            junctionSignPanel4_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel4_ELayout = new javax.swing.GroupLayout(junctionSignPanel4_E);
        junctionSignPanel4_E.setLayout(junctionSignPanel4_ELayout);
        junctionSignPanel4_ELayout.setHorizontalGroup(
            junctionSignPanel4_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel4_ELayout.setVerticalGroup(
            junctionSignPanel4_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel4Layout = new javax.swing.GroupLayout(junctionPanel4);
        junctionPanel4.setLayout(junctionPanel4Layout);
        junctionPanel4Layout.setHorizontalGroup(
            junctionPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel4Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel4_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel4_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel4_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel4_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel4Layout.setVerticalGroup(
            junctionPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel4Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel4_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel4_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel4Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel4_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel4_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField5.setEditable(false);
        junctionTextField5.setColumns(8);
        junctionTextField5.setText("Junction E");
        junctionTextField5.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel5_NLayout = new javax.swing.GroupLayout(junctionSignPanel5_N);
        junctionSignPanel5_N.setLayout(junctionSignPanel5_NLayout);
        junctionSignPanel5_NLayout.setHorizontalGroup(
            junctionSignPanel5_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel5_NLayout.setVerticalGroup(
            junctionSignPanel5_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel5_WLayout = new javax.swing.GroupLayout(junctionSignPanel5_W);
        junctionSignPanel5_W.setLayout(junctionSignPanel5_WLayout);
        junctionSignPanel5_WLayout.setHorizontalGroup(
            junctionSignPanel5_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel5_WLayout.setVerticalGroup(
            junctionSignPanel5_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel5_SLayout = new javax.swing.GroupLayout(junctionSignPanel5_S);
        junctionSignPanel5_S.setLayout(junctionSignPanel5_SLayout);
        junctionSignPanel5_SLayout.setHorizontalGroup(
            junctionSignPanel5_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel5_SLayout.setVerticalGroup(
            junctionSignPanel5_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel5_ELayout = new javax.swing.GroupLayout(junctionSignPanel5_E);
        junctionSignPanel5_E.setLayout(junctionSignPanel5_ELayout);
        junctionSignPanel5_ELayout.setHorizontalGroup(
            junctionSignPanel5_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel5_ELayout.setVerticalGroup(
            junctionSignPanel5_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel5Layout = new javax.swing.GroupLayout(junctionPanel5);
        junctionPanel5.setLayout(junctionPanel5Layout);
        junctionPanel5Layout.setHorizontalGroup(
            junctionPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel5Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel5_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel5_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel5_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addGap(50, 50, 50)
                        .addComponent(junctionSignPanel5_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel5Layout.setVerticalGroup(
            junctionPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel5Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel5_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField5, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel5_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel5Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel5_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel5_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField6.setEditable(false);
        junctionTextField6.setColumns(8);
        junctionTextField6.setText("Junction SW");
        junctionTextField6.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel6_NLayout = new javax.swing.GroupLayout(junctionSignPanel6_N);
        junctionSignPanel6_N.setLayout(junctionSignPanel6_NLayout);
        junctionSignPanel6_NLayout.setHorizontalGroup(
            junctionSignPanel6_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel6_NLayout.setVerticalGroup(
            junctionSignPanel6_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel6_WLayout = new javax.swing.GroupLayout(junctionSignPanel6_W);
        junctionSignPanel6_W.setLayout(junctionSignPanel6_WLayout);
        junctionSignPanel6_WLayout.setHorizontalGroup(
            junctionSignPanel6_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel6_WLayout.setVerticalGroup(
            junctionSignPanel6_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel6_SLayout = new javax.swing.GroupLayout(junctionSignPanel6_S);
        junctionSignPanel6_S.setLayout(junctionSignPanel6_SLayout);
        junctionSignPanel6_SLayout.setHorizontalGroup(
            junctionSignPanel6_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel6_SLayout.setVerticalGroup(
            junctionSignPanel6_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel6_ELayout = new javax.swing.GroupLayout(junctionSignPanel6_E);
        junctionSignPanel6_E.setLayout(junctionSignPanel6_ELayout);
        junctionSignPanel6_ELayout.setHorizontalGroup(
            junctionSignPanel6_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel6_ELayout.setVerticalGroup(
            junctionSignPanel6_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel6Layout = new javax.swing.GroupLayout(junctionPanel6);
        junctionPanel6.setLayout(junctionPanel6Layout);
        junctionPanel6Layout.setHorizontalGroup(
            junctionPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel6Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel6_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel6_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel6_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel6_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel6Layout.setVerticalGroup(
            junctionPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel6Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel6_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField6, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel6_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel6Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel6_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel6_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField7.setEditable(false);
        junctionTextField7.setColumns(8);
        junctionTextField7.setText("Junction S");
        junctionTextField7.setPreferredSize(new java.awt.Dimension(70, 70));
        junctionTextField7.setRequestFocusEnabled(false);

        javax.swing.GroupLayout junctionSignPanel7_NLayout = new javax.swing.GroupLayout(junctionSignPanel7_N);
        junctionSignPanel7_N.setLayout(junctionSignPanel7_NLayout);
        junctionSignPanel7_NLayout.setHorizontalGroup(
            junctionSignPanel7_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel7_NLayout.setVerticalGroup(
            junctionSignPanel7_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel7_WLayout = new javax.swing.GroupLayout(junctionSignPanel7_W);
        junctionSignPanel7_W.setLayout(junctionSignPanel7_WLayout);
        junctionSignPanel7_WLayout.setHorizontalGroup(
            junctionSignPanel7_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel7_WLayout.setVerticalGroup(
            junctionSignPanel7_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel7_SLayout = new javax.swing.GroupLayout(junctionSignPanel7_S);
        junctionSignPanel7_S.setLayout(junctionSignPanel7_SLayout);
        junctionSignPanel7_SLayout.setHorizontalGroup(
            junctionSignPanel7_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel7_SLayout.setVerticalGroup(
            junctionSignPanel7_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel7_ELayout = new javax.swing.GroupLayout(junctionSignPanel7_E);
        junctionSignPanel7_E.setLayout(junctionSignPanel7_ELayout);
        junctionSignPanel7_ELayout.setHorizontalGroup(
            junctionSignPanel7_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel7_ELayout.setVerticalGroup(
            junctionSignPanel7_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel7Layout = new javax.swing.GroupLayout(junctionPanel7);
        junctionPanel7.setLayout(junctionPanel7Layout);
        junctionPanel7Layout.setHorizontalGroup(
            junctionPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel7Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel7_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel7_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel7_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel7_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel7Layout.setVerticalGroup(
            junctionPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel7Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel7_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField7, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel7_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel7Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel7_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel7_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionTextField8.setEditable(false);
        junctionTextField8.setColumns(8);
        junctionTextField8.setText("Junction SE");
        junctionTextField8.setPreferredSize(new java.awt.Dimension(70, 70));

        javax.swing.GroupLayout junctionSignPanel8_NLayout = new javax.swing.GroupLayout(junctionSignPanel8_N);
        junctionSignPanel8_N.setLayout(junctionSignPanel8_NLayout);
        junctionSignPanel8_NLayout.setHorizontalGroup(
            junctionSignPanel8_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel8_NLayout.setVerticalGroup(
            junctionSignPanel8_NLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel8_WLayout = new javax.swing.GroupLayout(junctionSignPanel8_W);
        junctionSignPanel8_W.setLayout(junctionSignPanel8_WLayout);
        junctionSignPanel8_WLayout.setHorizontalGroup(
            junctionSignPanel8_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel8_WLayout.setVerticalGroup(
            junctionSignPanel8_WLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel8_SLayout = new javax.swing.GroupLayout(junctionSignPanel8_S);
        junctionSignPanel8_S.setLayout(junctionSignPanel8_SLayout);
        junctionSignPanel8_SLayout.setHorizontalGroup(
            junctionSignPanel8_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel8_SLayout.setVerticalGroup(
            junctionSignPanel8_SLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionSignPanel8_ELayout = new javax.swing.GroupLayout(junctionSignPanel8_E);
        junctionSignPanel8_E.setLayout(junctionSignPanel8_ELayout);
        junctionSignPanel8_ELayout.setHorizontalGroup(
            junctionSignPanel8_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );
        junctionSignPanel8_ELayout.setVerticalGroup(
            junctionSignPanel8_ELayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 20, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout junctionPanel8Layout = new javax.swing.GroupLayout(junctionPanel8);
        junctionPanel8.setLayout(junctionPanel8Layout);
        junctionPanel8Layout.setHorizontalGroup(
            junctionPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel8Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(junctionPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addComponent(junctionSignPanel8_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionSignPanel8_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addComponent(junctionSignPanel8_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addGap(54, 54, 54)
                        .addComponent(junctionSignPanel8_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );
        junctionPanel8Layout.setVerticalGroup(
            junctionPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(junctionPanel8Layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(junctionSignPanel8_N, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(junctionPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(junctionTextField8, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(junctionSignPanel8_W, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(junctionPanel8Layout.createSequentialGroup()
                        .addGap(31, 31, 31)
                        .addComponent(junctionSignPanel8_E, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(junctionSignPanel8_S, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        junctionVehicleField.setColumns(20);
        junctionVehicleField.setRows(5);
        junctionVehicleField.setEditable(false);
        jScrollPane1.setViewportView(junctionVehicleField);
        

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(junctionPanel0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(junctionPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(junctionPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(junctionPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(junctionPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(junctionPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(junctionPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(junctionPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(junctionPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(junctionPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(junctionPanel0, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(junctionPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(junctionPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(junctionPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(junctionPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(junctionPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(junctionPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(junctionPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
    } 
   
   public void initialiseSquareColor(Color c) {
       for(int i = 0; i < (junction_max * 4); i++) {
           setSquareColor(i, c);
       }
   }
   public void setSquareColor(int id, Color c) {
       junctionSignPanels[id].setBackground(c);
   }
   public void changeJunctionText(int id, String text) {
       junctionTexts[id].setText(text);   
   }
   
   public void resetVehicleText() {
       junctionVehicleField.setText("");
   }
   public void setVehicleText(String text) {
       junctionVehicleField.setText(text);
   }
   public void addVehicleText(String text) {
       junctionVehicleField.setText(text + 
                "\n" + junctionVehicleField.getText());   
   }
   
    private javax.swing.JPanel junctionPanel0;
    private javax.swing.JPanel junctionPanel1;
    private javax.swing.JPanel junctionPanel2;
    private javax.swing.JPanel junctionPanel3;
    private javax.swing.JPanel junctionPanel4;
    private javax.swing.JPanel junctionPanel5;
    private javax.swing.JPanel junctionPanel6;
    private javax.swing.JPanel junctionPanel7;
    private javax.swing.JPanel junctionPanel8;
    private javax.swing.JPanel junctionSignPanel0_E;
    private javax.swing.JPanel junctionSignPanel0_N;
    private javax.swing.JPanel junctionSignPanel0_S;
    private javax.swing.JPanel junctionSignPanel0_W;
    private javax.swing.JPanel junctionSignPanel1_E;
    private javax.swing.JPanel junctionSignPanel1_N;
    private javax.swing.JPanel junctionSignPanel1_S;
    private javax.swing.JPanel junctionSignPanel1_W;
    private javax.swing.JPanel junctionSignPanel2_E;
    private javax.swing.JPanel junctionSignPanel2_N;
    private javax.swing.JPanel junctionSignPanel2_S;
    private javax.swing.JPanel junctionSignPanel2_W;
    private javax.swing.JPanel junctionSignPanel3_E;
    private javax.swing.JPanel junctionSignPanel3_N;
    private javax.swing.JPanel junctionSignPanel3_S;
    private javax.swing.JPanel junctionSignPanel3_W;
    private javax.swing.JPanel junctionSignPanel4_E;
    private javax.swing.JPanel junctionSignPanel4_N;
    private javax.swing.JPanel junctionSignPanel4_S;
    private javax.swing.JPanel junctionSignPanel4_W;
    private javax.swing.JPanel junctionSignPanel5_E;
    private javax.swing.JPanel junctionSignPanel5_N;
    private javax.swing.JPanel junctionSignPanel5_S;
    private javax.swing.JPanel junctionSignPanel5_W;
    private javax.swing.JPanel junctionSignPanel6_E;
    private javax.swing.JPanel junctionSignPanel6_N;
    private javax.swing.JPanel junctionSignPanel6_S;
    private javax.swing.JPanel junctionSignPanel6_W;
    private javax.swing.JPanel junctionSignPanel7_E;
    private javax.swing.JPanel junctionSignPanel7_N;
    private javax.swing.JPanel junctionSignPanel7_S;
    private javax.swing.JPanel junctionSignPanel7_W;
    private javax.swing.JPanel junctionSignPanel8_E;
    private javax.swing.JPanel junctionSignPanel8_N;
    private javax.swing.JPanel junctionSignPanel8_S;
    private javax.swing.JPanel junctionSignPanel8_W;
    private javax.swing.JTextArea junctionTextField0;
    private javax.swing.JTextArea junctionTextField1;
    private javax.swing.JTextArea junctionTextField2;
    private javax.swing.JTextArea junctionTextField3;
    private javax.swing.JTextArea junctionTextField4;
    private javax.swing.JTextArea junctionTextField5;
    private javax.swing.JTextArea junctionTextField6;
    private javax.swing.JTextArea junctionTextField7;
    private javax.swing.JTextArea junctionTextField8;
    private javax.swing.JTextArea junctionVehicleField;
    private javax.swing.JScrollPane jScrollPane1;
}