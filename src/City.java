import jade.core.AID;
import jade.core.behaviours.CyclicBehaviour;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.wrapper.ContainerController;
import jade.lang.acl.ACLMessage;
import java.util.LinkedList;
import java.util.Random;
import java.awt.Color;

/**
 * Trieda, popisujuca hlavneho agenta, "Mesto", ktory vytvara a zastresuje ostatnych agentov.
 * @author Supinic
 */

public class City extends GuiAgent {

	protected ContainerController control;

	private LinkedList<AID> junctionList;
        
        private final int junction_array_width = 3;
        private final int junction_array_height = 3;
        
	private final int junctions = junction_array_width * junction_array_height,
                          ways = 5;
	protected final int max_vehicles = 250;
        protected int max_queue = 0;
        private boolean basicLoad;
	
	protected final String JUNCTION_CLASS_NAME = "Junction";
	protected final String JUNCTION_AGENT_NAME = "Junction";
	protected final String VEHICLE_CLASS_NAME = "Vehicle";
	protected final String VEHICLE_AGENT_NAME = "Vehicle";
	
	protected int actual_vehicles = 0;
	protected int actual_vehicle_id = 0;
    protected int gone_vehicles = 0;
        protected int runningJunctions = 0;
        protected float noJunctions[] = {0,0,0};
        protected int gone[] = {0,0,0}, jun_max[] = {0,0,0,0,0,0,0,0,0}, jun_ave[] = {0,0,0,0,0,0,0,0,0};
        protected long spentTime[] = {0,0,0};
        public final int NO_PRIORITY = 1,
                         MHD = 10,
                         EMERGENCY = 100;
	
        protected ACLMessage send_message;
        protected ACLMessage recv_message;
        protected Random random;
        
        transient protected GUICityClass myGui;
        
    protected void onGuiEvent(GuiEvent ev) {
       int command = ev.getType();
       // Tu bude komunikacia s GUI
    }
        
	protected void setup() {
		System.out.println("Hello everyone! I am the master city agent.");
		
		junctionList = new LinkedList<AID>();
		basicLoad = false;
		control = getContainerController();
		for(int i = 0; i < junctions; i++) { // Dynamicke vytvorenie agentov Krizovatka
			try {
                             control.createNewAgent(JUNCTION_AGENT_NAME + (i+1), JUNCTION_CLASS_NAME, 
                                     new Object[] {
                                         new Integer(i), 
                                         new String(getAID().getLocalName()),
                                         new Integer(junction_array_height),
                                         new Integer(junction_array_width)
                                     }).start();
			}
			catch(Exception e) {
                            System.err.println("Exception occured while creating " + JUNCTION_AGENT_NAME);
                        }
			junctionList.add(getAID(JUNCTION_AGENT_NAME + (i+1)));
		}
		addBehaviour(new CityLoopBehaviour());
                
        random = new Random();
        myGui = new GUICityClass(this, junctions);
        myGui.setVisible(true);

        myGui.initialiseSquareColor(Color.red);
	}
	
	class CityLoopBehaviour extends CyclicBehaviour {

            
            public void askForNeighbors() {
                runningJunctions = 0;
                //reknu vsem krizovatkam, aby si zjistily navazujici semafory
                for(int i = 0; i < junctions; i++){
                    send_message = new ACLMessage(ACLMessage.INFORM);
                    send_message.addReceiver(junctionList.get(i));
                    send_message.setContent("Najdi nasledujici semafory.");
                    myAgent.send(send_message);
                }
            }
       
            @Override
            public void action() {          
                
                ACLMessage recv_message = myAgent.receive();
                    if (recv_message != null) {
			//System.out.println("Received message from agent "+recv_message.getSender().getLocalName());
			/** TYP_ZPRAVY(odesilatel): vyznam
			 * CANCEL(vozidlo): Vozidlo spachalo sebevrazdu, mesto moze vytvorit nove vozidlo.
                         * INFORM(krizovatka): Najdi si nasledne semafory svych semaforu
                         * INFORM(vozidlo): inicializace prvniho vyskytu
                         * AGREE(krizovatka): jsem vyvorena a mohu prijimat rozkazy
			 */
                        
                        if(recv_message.getPerformative() == ACLMessage.AGREE) {
                            runningJunctions++;
                            if(runningJunctions == junctions){
                                askForNeighbors();
                            }
                        }
                        if(recv_message.getPerformative() == ACLMessage.CONFIRM) {
                            runningJunctions++;
                            if(runningJunctions == junctions){
                                basicLoad = true;
                            }
                        }
			if(recv_message.getPerformative() == ACLMessage.CANCEL) {
                            String[] vals = recv_message.getContent().split(";");
                            int prior = Integer.parseInt(vals[0]), priority;
                            switch(prior){
                                default: 
                                    priority = 0;
                                    break;
                                case MHD:
                                    priority = 1;
                                    break;
                                case EMERGENCY:
                                    priority = 2;
                                    break;
                            }
                            //System.out.println(prior + "  " + priority);
                            gone[priority]++;
                            noJunctions[priority] = noJunctions[priority] + (Float.parseFloat(vals[1]));
                            spentTime[priority] += Long.parseLong(vals[2]);
                            gone_vehicles++;
                            actual_vehicles--;	
                        }
                        if(recv_message.getPerformative() == ACLMessage.PROPAGATE) {
                            String[] vals = recv_message.getContent().split(";");
                            jun_max[Integer.parseInt(vals[8])] = Integer.parseInt(vals[9]);
                            jun_ave[Integer.parseInt(vals[8])] = Integer.parseInt(vals[10]);
                            for(int i = 0; i < 8; i = i+2){
                                int id = Integer.parseInt(vals[i]);
                                if(vals[i+1].equals("0")){
                                    myGui.setSquareColor(id,Color.red);
                                }else{
                                    myGui.setSquareColor(id,Color.green);
                                }
                            }
                            
                            myGui.setVehicleText("Car left: " + gone_vehicles + "\n" +
                                                 "Average no visited junctions:\n"+
                                                 "   No priority: " + String.format("%.3f",(gone[0] > 0 ? noJunctions[0]/gone[0]: 0f)) + "\n" +
                                                 "   MHD: " + String.format("%.3f",(gone[1] > 0 ? noJunctions[1]/gone[1]: 0f)) + "\n" +
                                                 "   Emergency: " + String.format("%.3f",(gone[2] > 0 ? noJunctions[2]/gone[2]: 0f)) + "\n\n" +
                                                 "Average time of vehicles in system:\n"+
                                                 "   No priority: " + (gone[0] > 0?spentTime[0]/gone[0]:0) + "\n" +
                                                 "   MHD: " + (gone[1] > 0?spentTime[1]/gone[1]:0) + "\n" +
                                                 "   Emergency: " + (gone[2] > 0?spentTime[2]/gone[2]:0) + "\n\n" +
                                                 "Max queue length:\n"+
                                                 "   Junction 1: " + jun_max[0] + "\n" +
                                                 "   Junction 2: " + jun_max[1] + "\n" +
                                                 "   Junction 3: " + jun_max[2] + "\n" +
                                                 "   Junction 4: " + jun_max[3] + "\n" +
                                                 "   Junction 5: " + jun_max[4] + "\n" +
                                                 "   Junction 6: " + jun_max[5] + "\n" +
                                                 "   Junction 7: " + jun_max[6] + "\n" +
                                                 "   Junction 8: " + jun_max[7] + "\n" +
                                                 "   Junction 9: " + jun_max[8] + "\n\n" + 
                                                 "Average queue length:\n"+
                                                 "   Junction 1: " + jun_ave[0] + "\n" +
                                                 "   Junction 2: " + jun_ave[1] + "\n" +
                                                 "   Junction 3: " + jun_ave[2] + "\n" +
                                                 "   Junction 4: " + jun_ave[3] + "\n" +
                                                 "   Junction 5: " + jun_ave[4] + "\n" +
                                                 "   Junction 6: " + jun_ave[5] + "\n" +
                                                 "   Junction 7: " + jun_ave[6] + "\n" +
                                                 "   Junction 8: " + jun_ave[7] + "\n" +
                                                 "   Junction 9: " + jun_ave[8] + "\n\n");
                        }
                        if(recv_message.getPerformative() == ACLMessage.INFORM_REF) {
                            String[] vals = recv_message.getContent().split(";");
                            int id = Integer.parseInt(vals[0]);
                            myGui.changeJunctionText(id,vals[1]);
                        }
                    } else {
                        if(actual_vehicles < max_vehicles && basicLoad) {
                            try {
                                // Mesto vytvori nove vozidlo, a nahodne mu priradi AID krizovatky, na ktorej sa vygeneruje
                                int priority = random.nextInt(101);
                                if(priority > 95)
                                    priority = EMERGENCY;
                                else if(priority > 80)
                                    priority = MHD;
                                else priority = NO_PRIORITY;
                                
                                control.createNewAgent(VEHICLE_AGENT_NAME + actual_vehicle_id, VEHICLE_CLASS_NAME, new Object[]{new Integer(priority), new String(getAID().getLocalName())}).start();
                                send_message = new ACLMessage(ACLMessage.INFORM);
                                int random_junction = (random.nextInt(junctions)) + 1,
                                    random_light = (random.nextInt(ways-1)) + 1;
                                send_message.setContent(random_junction + "" + random_light);
                                send_message.addReceiver(getAID(VEHICLE_AGENT_NAME + actual_vehicle_id));
                                myAgent.send(send_message);
                            }
                            catch(Exception e) {
                                System.err.println("Exception occured while creating " + VEHICLE_AGENT_NAME);
                            }
                            actual_vehicles++;
                            actual_vehicle_id++;
                        } 
                    }
            }
	}
}