
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.core.behaviours.TickerBehaviour;
import jade.lang.acl.ACLMessage;
import jade.wrapper.ContainerController;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Jozkar
 */
public class Junction extends Agent {
    
    protected int my_id, rows, cols, answered_semaphores, tick = 0,noAnsw = 0, avg = 0;
    protected float prioThreshold = 10;
    protected String city;
    protected AID[] my_junction_neighbors;
    protected int[] junction_neighbors_id = {0,0,0,0}, semaphores_id = {0,0,0,0}, semaphoresPrios = {0,0,0,0}, max={0,0,0,0};
    protected float [] neighbors_status = {1,1,1,1};
    protected AID[] my_semaphores;
    protected String[] content = {"","","",""};
    
    protected final String SEMAPHORE_AGENT_NAME = "Light";
    protected final String JUNCTION_AGENT_NAME = "Junction";
    protected final String SEMAPHORE_CLASS_NAME = "TraficLight";
    protected final int NORTH = 0, SOUTH = 2, EAST = 3, WEST = 1;
    protected boolean horizontalActive = false; // urceni ktere dva semafory jsou nyni v zelene
    
    protected ContainerController control;
    protected boolean do_once = true,
                      done,
                      onFirstTick = true;
    
    protected void switchTrafficLights(boolean horizontal, Agent myAgent) {
        ACLMessage toLights, toCity;
        //System.out.println("Junction no." + my_id + ": Horizontal way is now RED; Vertical way is now GREEN.");
        
        toLights = new ACLMessage(horizontal ? ACLMessage.ACCEPT_PROPOSAL : ACLMessage.REJECT_PROPOSAL);
        toLights.addReceiver(my_semaphores[EAST]);
        toLights.addReceiver(my_semaphores[WEST]);
        myAgent.send(toLights);

        toLights = new ACLMessage(!horizontal ? ACLMessage.ACCEPT_PROPOSAL : ACLMessage.REJECT_PROPOSAL);
        toLights.addReceiver(my_semaphores[NORTH]);
        toLights.addReceiver(my_semaphores[SOUTH]);
        myAgent.send(toLights);

        int maximal = 0;
        int average = avg / noAnsw;
        for(int i = 0; i < 4; i++){
            if(max[i] > maximal){
                maximal = max[i];
            }
        }
        toCity = new ACLMessage(ACLMessage.PROPAGATE);
        toCity.addReceiver(getAID(city));
        if(horizontal)
            toCity.setContent(semaphores_id[EAST] + ";" + "1" + ";" +
                        semaphores_id[WEST] + ";" + "1" + ";" +
                        semaphores_id[NORTH] + ";" + "0" + ";" +
                        semaphores_id[SOUTH] + ";" + "0;" + my_id + ";" + maximal + ";" + average);
        else toCity.setContent(semaphores_id[EAST] + ";" + "0" + ";" +
                        semaphores_id[WEST] + ";" + "0" + ";" +
                        semaphores_id[NORTH] + ";" + "1" + ";" +
                        semaphores_id[SOUTH] + ";" + "1;" + my_id + ";" + maximal + ";" + average);
        myAgent.send(toCity);
    }
    
    // Parameter pri vytvoreni: my_id
    protected void setup() {
  	//System.out.println("Hello city! I am junction no "+getLocalName()+ " in our beautifull city!");
        my_junction_neighbors = new AID[4];
        my_semaphores = new AID[4];
        control = getContainerController();
        done = false;
        
        Object[] args = getArguments();
        my_id = ((Integer)args[0]).intValue();
        city = ((String)args[1]);
        rows = ((Integer)args[2]).intValue();
        cols = ((Integer)args[3]).intValue();
        answered_semaphores = 0;
        
        for(int i = 0; i < 4; i++) {
             try {
                control.createNewAgent(SEMAPHORE_AGENT_NAME + (my_id + 1) + "" + (i+1), SEMAPHORE_CLASS_NAME, new Object[] {new Integer(my_id), new Integer(i)}).start();
              } catch (Exception ex) {
                  Logger.getLogger(Junction.class.getName()).log(Level.SEVERE, null, ex);
              }
             my_semaphores[i] = getAID(SEMAPHORE_AGENT_NAME + (my_id + 1) + (i+1));
             semaphores_id[i] = 4 * my_id + i;
        
        }
        addBehaviour(new RcvJunctionMsgBehaviour());
        
        addBehaviour(new TickerBehaviour(this, 5000){ //CHYTRISTIKA
            protected void onTick(){    //TODO?: mozna by mezi prepnutim melo byt zpozdeni - problem je ze po tuto dobu by krizovatka asi neprijmala zpravy :(
                
                float diff = (semaphoresPrios[EAST]*neighbors_status[EAST] + semaphoresPrios[WEST]*neighbors_status[WEST]) - (semaphoresPrios[NORTH]*neighbors_status[NORTH] + semaphoresPrios[SOUTH]*neighbors_status[SOUTH]); 
                prioThreshold = Math.abs(diff) / 4;
                if(horizontalActive && (diff + prioThreshold) < 0) { // semaforum E a W posilame cervenou a N a S zelenou
                    //System.out.println("Junction no." + my_id + ": Horizontal way is now RED; Vertical way is now GREEN.");
                    switchTrafficLights(false, myAgent);
                    horizontalActive = false;
                }
                else if(!horizontalActive && (diff - prioThreshold) > 0){ // semaforum N a S posilame cervenou a E a W zelenou
                    //System.out.println("Junction no." + my_id + ": Horizontal way is now GREEN; Vertical way is now RED.");
                    switchTrafficLights(true, myAgent);
                    horizontalActive = true;
                }
                else if(onFirstTick) {
                    //System.out.println("Junction no." + my_id + ": Horizontal way is now GREEN; Vertical way is now RED.");
                    switchTrafficLights(true, myAgent);
                    horizontalActive = true;
                    onFirstTick = false;
                }
                done = false;      
            }
        });
        
        /*
        addBehaviour(new TickerBehaviour(this, 15000){ //HLOUPE CHOVANI
            protected void onTick(){    //TODO?: mozna by mezi prepnutim melo byt zpozdeni - problem je ze po tuto dobu by krizovatka asi neprijmala zpravy :(
                if(horizontalActive) { // semaforum E a W posilame cervenou a N a S zelenou
                    //System.out.println("Junction no." + my_id + ": Horizontal way is now RED; Vertical way is now GREEN.");
                    switchTrafficLights(false, myAgent);
                    
                }
                else { // semaforum N a S posilame cervenou a E a W zelenou
                    //System.out.println("Junction no." + my_id + ": Horizontal way is now GREEN; Vertical way is now RED.");
                    switchTrafficLights(true, myAgent);
                }
                done = false;
                horizontalActive = !horizontalActive;
            }
        });
        */
        
        addBehaviour(new TickerBehaviour(this, 200){
            protected void onTick(){    //TODO?: mozna by mezi prepnutim melo byt zpozdeni - problem je ze po tuto dobu by krizovatka asi neprijmala zpravy :(
                ACLMessage toAllLights = new ACLMessage(ACLMessage.REQUEST);
                toAllLights.addReceiver(my_semaphores[NORTH]);
                toAllLights.addReceiver(my_semaphores[SOUTH]);
                toAllLights.addReceiver(my_semaphores[EAST]);
                toAllLights.addReceiver(my_semaphores[WEST]);
                myAgent.send(toAllLights);    
                tick++;
                
                if(tick > 4){
                    tick = 0;
                    ACLMessage toAroundJunctions = new ACLMessage(ACLMessage.REQUEST);
                    for(int i = 0; i < 4; i++){
                        if(my_junction_neighbors[i] != null){
                            toAroundJunctions.addReceiver(my_junction_neighbors[i]);
                        }
                    }
                    myAgent.send(toAroundJunctions);
                }
            }
        });
    }
    

    
    class RcvJunctionMsgBehaviour extends CyclicBehaviour {
        public void action() { 
            
            if(do_once) {
                //ohlasim se mestu, ze uz ziji
                ACLMessage toCity = new ACLMessage(ACLMessage.AGREE);
                toCity.addReceiver(getAID(city));
                myAgent.send(toCity);
                do_once = false;
            }
            /*if(done && answered_semaphores == 0){
                ACLMessage toLights = new ACLMessage(ACLMessage.REQUEST);
                toLights.addReceiver(my_semaphores[NORTH]);
                toLights.addReceiver(my_semaphores[SOUTH]);
                toLights.addReceiver(my_semaphores[EAST]);
                toLights.addReceiver(my_semaphores[WEST]);
                myAgent.send(toLights);                    
                done = false;
            }*/
            
            /**
             * TYP_ZPRAVY(odesilatel): vyznam
             * INFORM(city): najdi si sve krizovatkove sousedy
             *   
             */
            
            ACLMessage msg = myAgent.receive(); 

            if(msg != null){
                if(msg.getPerformative() == ACLMessage.REQUEST){
                    ACLMessage response = new ACLMessage(ACLMessage.INFORM_IF);
                    response.addReceiver(msg.getSender());
                    response.setContent(my_id+";"+(horizontalActive?"1":"0"));
                    myAgent.send(response);
                }
                if(msg.getPerformative() == ACLMessage.INFORM){
                    int row = my_id / rows;
                    int col = my_id % cols;
                    int position;
                    
                    if(row == 0){
                        my_junction_neighbors[NORTH] = null;
                    }else{
                        junction_neighbors_id[NORTH] = (rows * (row-1)) + col + 1;
                        my_junction_neighbors[NORTH] = getAID(JUNCTION_AGENT_NAME + junction_neighbors_id[NORTH]);
                    }
                    
                    if(row == (rows-1)){
                        my_junction_neighbors[SOUTH] = null;
                    }else{
                        junction_neighbors_id[SOUTH]  = (rows * (row+1)) + col + 1;
                        my_junction_neighbors[SOUTH] = getAID(JUNCTION_AGENT_NAME + junction_neighbors_id[SOUTH]);
                    }
                    
                    if(col == 0){
                        my_junction_neighbors[WEST] = null;
                    }else{
                        junction_neighbors_id[WEST] = (rows * row) + col;
                        my_junction_neighbors[WEST] = getAID(JUNCTION_AGENT_NAME + junction_neighbors_id[WEST]);
                    }
                    
                    if(col == (cols-1)){
                        my_junction_neighbors[EAST] = null;
                    }else{
                        junction_neighbors_id[EAST] = (rows * row) + col + 2;
                        my_junction_neighbors[EAST] = getAID(JUNCTION_AGENT_NAME + junction_neighbors_id[EAST]);
                    }
                    
                    //System.out.println("Junction no." + my_id + " has generated all neighbors.");
                    
                    ACLMessage toLights;
                    for(int i = 0; i < 4; i++){
                        toLights = new ACLMessage(ACLMessage.INFORM);
                        toLights.addReceiver(my_semaphores[i]);
                        toLights.setContent( junction_neighbors_id[NORTH]+";"
                                            +junction_neighbors_id[SOUTH]+";"
                                            +junction_neighbors_id[EAST]+";"
                                            +junction_neighbors_id[WEST]);
                        myAgent.send(toLights);
                    }
                    //System.out.println("Junction no." + my_id + " has sent info to all her traffic lights.");
                    ACLMessage toCity = new ACLMessage(ACLMessage.CONFIRM);
                    toCity.addReceiver(getAID(city));
                    myAgent.send(toCity);
                    done = true;
                    
                }
                if(msg.getPerformative() == ACLMessage.INFORM_REF){
                    
                    answered_semaphores++;
                    String [] vals = msg.getContent().split(";");
                    int way = Integer.parseInt(vals[0]);
                    content[way] = vals[1] + " / " + vals[2];
                    if(max[way] < Integer.parseInt(vals[2])){
                        max[way] = Integer.parseInt(vals[2]);
                    }
                    noAnsw++;
                    avg += Integer.parseInt(vals[2]);
                    semaphoresPrios[way] = Integer.parseInt(vals[1]);
                    if(answered_semaphores > 3){
                        answered_semaphores = 0;
                        ACLMessage toCity = new ACLMessage(ACLMessage.INFORM_REF);
                        toCity.addReceiver(getAID(city));
                        toCity.setContent(my_id + ";N: " + content[NORTH] + "\n" +
                                          "E: " + content[EAST] + "\n" +
                                          "S: " + content[SOUTH] + "\n" +
                                          "W: " + content[WEST]);
                        myAgent.send(toCity);
                    }
                }
                if(msg.getPerformative() == ACLMessage.INFORM_IF){
                    String [] vals = msg.getContent().split(";");
                    int id = Integer.parseInt(vals[0]),
                        state = Integer.parseInt(vals[1]);
                    for(int i = 0; i < 4; i++){
                        if(junction_neighbors_id[i] == id){
                            switch(i){
                                case NORTH:
                                    neighbors_status[i] = (state == 0? 1.05f : 1f);
                                    break;
                                case SOUTH:
                                    neighbors_status[i] = (state == 0? 1.05f : 1f);
                                    break;
                                case EAST:
                                    neighbors_status[i] = (state == 1? 1.05f : 1f);
                                    break;
                                case WEST:
                                    neighbors_status[i] = (state == 1? 1.05f : 1f);
                                    break;
                            }
                        }                        
                    }
                }
            }
            else block();
            
            
        }
    }
}