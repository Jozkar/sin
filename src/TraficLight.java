
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import java.util.LinkedList;



/**
 * Trida popisujici chovani agenta reprezentujici semafor.
 * @author Lox
 */
public class TraficLight extends Agent {
    
    public boolean state, driveThrough;           // false = cervena
    private int position, junctionID;
    private LinkedList queue;       // fronta cekajicich vozidel
    private int queueImportance;    // celkova vaha/dulezitost fronty
    private String[] nextWay;
    protected final String SEMAPHORE_AGENT_NAME = "Light";
    private static final int NORTH = 0, SOUTH = 2, EAST = 3, WEST = 1, LEFT = 0, STRAIGHT = 1, RIGHT = 2;
    
    protected void setup() {
  	
        state = false;
        driveThrough = false;
        queue = new LinkedList<AID>();
        queueImportance = 0;
        nextWay = new String[3];
        Object[] args = getArguments();
        junctionID = ((Integer)args[0]).intValue();
        position = ((Integer)args[1]).intValue();
                
        //System.out.println(getAID().getLocalName() + " is alive !!");
        
        addBehaviour(new RcvMsgBehaviour());
    }
    
    class RcvMsgBehaviour extends CyclicBehaviour {
        public void action() { 
            ACLMessage msg = myAgent.receive(); 

            if(msg == null) {
                block();
            }
            else { 
                /**  TYP_ZPRAVY(odesilatel): vyznam
                 * SUBSCRIBE(vozidlo): vozidlo prijelo k semaforu
                 * CONFIRM(vozidlo): vozidlo opustilo semafor
                 * REQUEST(krizovatka): krizovatka pozaduje informace o fronte
                 * ACCEPT_PROPOSAL(krizovatka): krizovatka nastavuje semafor na zelenou
                 * REJECT_PROPOSAL(krizovatka): krizovatka nastavuje semafor na cervenou
                 * INFORM(krizovatka): nageneruj si na jake semafory muze auto z tebe jet
                */
                if(msg.getPerformative() == ACLMessage.SUBSCRIBE) { //vozidlo-semafor; vozidlo prijelo k semaforu
                    queue.add(msg.getSender());
                    String [] info = msg.getContent().split(";");
                    int way = Integer.parseInt(info[0]);
                    queueImportance += Integer.parseInt(info[1]);
                    
                    ACLMessage sendNextWay = new ACLMessage(ACLMessage.AGREE);
                    sendNextWay.addReceiver(msg.getSender());
                    sendNextWay.setContent(nextWay[way]);
                    myAgent.send(sendNextWay);
                    
                    if(state && !driveThrough) {
                        releaseFirstVehicle();
                    }
                }
                else if(msg.getPerformative() == ACLMessage.CONFIRM) { //vozidlo-semafor; vozidlo opustilo semafor/frontu
          //          System.out.println(myAgent.getLocalName() + ": " + msg.getSender().getLocalName() + " - doruceny CONFIRM - bude se mazat");
          //          System.out.println(myAgent.getLocalName() + ": removing "+ queue.removeFirst());
                    queue.removeFirst();
                    queueImportance -= Integer.parseInt(msg.getContent()); // TODO: resit priority vozidel
                    //System.out.println(msg.getContent());
                    if(state && queueImportance > 0) {
                        releaseFirstVehicle();
                    }
                    else driveThrough = false;
                }
                else if(msg.getPerformative() == ACLMessage.REQUEST) { //krizovatka-semafor; krizovatka chce znat informace o fronte
                    ACLMessage response;    //semafor-krizovatka; semafor informuje krizovatku o stavu fronty
                    response = new ACLMessage(ACLMessage.INFORM_REF); 
                    response.setContent(position + ";" + queueImportance + ";" + queue.size());
                    response.addReceiver(msg.getSender()); 
                    myAgent.send(response);
                }
                else if(msg.getPerformative() == ACLMessage.ACCEPT_PROPOSAL) { //krizovatka-semafor; semafor ma nastavit zelenou
                    state = true;
                    if(!driveThrough)
                        releaseFirstVehicle();
                }
                else if(msg.getPerformative() == ACLMessage.REJECT_PROPOSAL) { //krizovatka-semafor; semafor ma nastavit cervenou
                    state = false;
                    driveThrough = false;
                }
                else if(msg.getPerformative() == ACLMessage.INFORM) { //krizovatka-semafor; semafor si zjisti nasledne semafory
                    String[] junctIDs = msg.getContent().split(";");
                    switch(position){
                        case NORTH:
                            nextWay[LEFT] = (junctIDs[EAST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[EAST] + (WEST+1));
                            nextWay[STRAIGHT] = (junctIDs[SOUTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[SOUTH] + (NORTH+1));
                            nextWay[RIGHT] = (junctIDs[WEST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[WEST] + (EAST+1));
                            break;
                        case SOUTH:
                            nextWay[RIGHT] = (junctIDs[EAST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[EAST] + (WEST+1));
                            nextWay[STRAIGHT] = (junctIDs[NORTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[NORTH] + (SOUTH+1));
                            nextWay[LEFT] = (junctIDs[WEST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[WEST] + (EAST+1));
                            break;
                        case EAST:
                            nextWay[STRAIGHT] = (junctIDs[WEST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[WEST] + (EAST+1));
                            nextWay[LEFT] = (junctIDs[SOUTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[SOUTH] + (NORTH+1));
                            nextWay[RIGHT] = (junctIDs[NORTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[NORTH] + (SOUTH+1));
                            break;
                        case WEST:
                            nextWay[LEFT] = (junctIDs[NORTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[NORTH] + (SOUTH+1));
                            nextWay[STRAIGHT] = (junctIDs[EAST].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[EAST] + (WEST+1));
                            nextWay[RIGHT] = (junctIDs[SOUTH].equals("0")? "exit" : SEMAPHORE_AGENT_NAME + junctIDs[SOUTH] + (NORTH+1));
                            break;
                    }
                    //System.out.println("I am " + getAID().getLocalName() + " and I had generated references to next lights.");
                }
            }
        }
        
        private void releaseFirstVehicle() { //semafor-vozidlo; semafor informuje vozidlo, ze muze projet
            if(queue.isEmpty()) {
                driveThrough = false;
                return;
            }
            
            driveThrough = true;
            ACLMessage msg = new ACLMessage(ACLMessage.CANCEL); 
            msg.setContent("Hotovo");
            msg.addReceiver((AID)queue.getFirst()); 
            myAgent.send(msg);
        }
    } 
}



